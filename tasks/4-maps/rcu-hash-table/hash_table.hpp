#pragma once

#include "rcu_lock.hpp"
#include "spinlock.hpp"

#include <twist/support/locking.hpp>

#include <functional>
#include <vector>

namespace solutions {

template <typename Key, typename Value>
class FixedSizeHashTable {
 private:
  class Bucket {
    // Your code goes here
  };

 public:
  FixedSizeHashTable(size_t num_buckets) : buckets_(num_buckets) {
  }

  bool Insert(const Key& key, const Value& value) {
    return false;  // Your code goes here
  }

  bool Remove(const Key& key) {
    return false;  // Your code goes here
  }

  // Wait-free
  bool Lookup(const Key& key, Value& value) {
    return false;  // Your code goes here
  }

 private:
  std::vector<Bucket> buckets_;
};

}  // namespace solutions
