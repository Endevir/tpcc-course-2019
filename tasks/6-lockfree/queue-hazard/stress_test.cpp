#include <lfqueue.hpp>

#include <twist/fault/adversary.hpp>
#include <twist/fault/lockfree.hpp>

#include <twist/ratelimit/quiescent.hpp>

#include <twist/test_framework/test_framework.hpp>
#include <twist/threading/test.hpp>

#include <twist/threading/spin_wait.hpp>

#include <twist/test_utils/executor.hpp>
#include <twist/test_utils/barrier.hpp>
#include <twist/test_utils/xor_checksum.hpp>

#include <twist/support/random.hpp>

#include <atomic>
#include <chrono>
#include <thread>
#include <vector>

//////////////////////////////////////////////////////////////////////

template <typename T>
struct Counted {
  static std::atomic<size_t> obj_count;
  static size_t obj_count_limit;

  Counted() {
    IncrementCount();
  }

  Counted(const Counted& /*that*/) {
    IncrementCount();
  }

  Counted(Counted&& /*that*/) {
    IncrementCount();
  }

  Counted& operator=(const Counted& that) = default;
  Counted& operator=(Counted&& that) = default;

  ~Counted() {
    DecrementCount();
  }

  static void SetLimit(size_t count) {
    obj_count_limit = count;
  }

  static size_t ObjectCount() {
    return obj_count.load();
  }

 private:
  static void IncrementCount() {
    ASSERT_TRUE_M(
      obj_count.fetch_add(1) + 1 < obj_count_limit,
      "Too many alive test objects: " << obj_count.load());
  }

  static void DecrementCount() {
    obj_count.fetch_sub(1);
  }
};

template <typename T>
std::atomic<size_t> Counted<T>::obj_count = 0;

template <typename T>
size_t Counted<T>::obj_count_limit = std::numeric_limits<size_t>::max();

//////////////////////////////////////////////////////////////////////

struct TestObject: public Counted<TestObject> {
  TestObject(const size_t thread_index, const size_t index)
      : thread_index_(thread_index),
        index_(index),
        payload_(std::to_string(index)) {
  }

  TestObject() {
  }

  size_t thread_index_{0};
  size_t index_{0};
  std::string payload_{};
};

class ThreadInsertsIterator {
 public:
  ThreadInsertsIterator(size_t inserts, size_t threads, size_t thread_index)
    : next_(thread_index),
      end_(inserts),
      step_(threads) {
  }

  bool HasNext() const {
    return next_ < end_;
  }

  size_t NextValue() {
    size_t value = next_;
    next_ += step_;
    return value;
  }

 private:
  size_t next_;
  size_t end_;
  size_t step_;
};

class Tester {
 public:
  Tester(const TTestParameters& parameters)
      : parameters_(parameters),
        start_barrier_(parameters_.Get(0)),
        rate_limiter_(parameters_.Get(0), std::chrono::microseconds(10)) {
  }

  void Run() {
    RunTestThreads();
    ValidateFinalInvariants();
  }

 private:
  void RunTestThreads() {
    TestObject::SetLimit(1000);

    twist::ScopedExecutor executor;
    for (size_t t = 0; t < parameters_.Get(0); ++t) {
      executor.Submit(&Tester::RunTestThread, this, t);
    }
    executor.Join();

    rate_limiter_.Stop();
  }

  void RunTestThread(const size_t thread_index) {
    auto* adversary = twist::fault::GetAdversary();

    size_t threads = parameters_.Get(0);
    size_t inserts = parameters_.Get(1);
    size_t batch_limit = parameters_.Get(2);

    std::vector<size_t> prev_object_indices(threads, (size_t)-1);

    start_barrier_.PassThrough();

    adversary->Enter();

    ThreadInsertsIterator thread_inserts(inserts, threads, thread_index);

    while (thread_inserts.HasNext()) {
      const size_t curr_batch_limit = twist::RandomUInteger(1, batch_limit);

      // Enqueues

      size_t batch_size = 0;
      while (batch_size < curr_batch_limit && thread_inserts.HasNext()) {
        rate_limiter_.Enter();

        auto value = thread_inserts.NextValue();
        queue_.Enqueue(TestObject(thread_index, value));

        rate_limiter_.Exit();
        checksum_.Feed(value);

        adversary->ReportProgress();
        ++batch_size;
      }

      // Dequeues

      for (size_t k = 0; k < batch_size; ++k) {
        rate_limiter_.Enter();

        TestObject object;
        ASSERT_TRUE_M(queue_.Dequeue(object), "Non-empty queue expected");

        rate_limiter_.Exit();

        adversary->ReportProgress();

        const size_t prev_object_index =
            prev_object_indices[object.thread_index_];
        const size_t curr_object_index = object.index_;

        if (prev_object_index != (size_t)-1) {
          ASSERT_TRUE_M(curr_object_index > prev_object_index,
                        "FIFO order violated");
        }

        prev_object_indices[object.thread_index_] = curr_object_index;

        checksum_.Feed(object.index_);
      }
    }

    adversary->Exit();
  }

  void ValidateFinalInvariants() {
    {
      TestObject object;
      ASSERT_FALSE_M(queue_.Dequeue(object), "Empty queue expected");
    }

    ASSERT_TRUE(checksum_.Validate());
  }

 private:
  TTestParameters parameters_;

  twist::OnePassBarrier start_barrier_;

  twist::ratelimit::QuiescentRateLimiter rate_limiter_;

  solutions::LockFreeQueue<TestObject> queue_;
  twist::XORCheckSum checksum_;
};



void StressTest(TTestParameters parameters) {
  Tester(parameters).Run();
}

// Test parameters: threads, inserts, batch limit

T_TEST_CASES(StressTest)
  .TimeLimit(std::chrono::minutes(1))
  .Case({3, 100000, 3})
  .Case({5, 100000, 10})
  .TimeLimit(std::chrono::seconds(90))
  .Case({10, 100000, 5})
  .TimeLimit(std::chrono::minutes(2)) // Too slow with TSan
  .Case({15, 100000, 3});

#if defined(TWIST_FIBER)

T_TEST_CASES(StressTest)
  .TimeLimit(std::chrono::minutes(1))
  .Case({25, 500000, 1})
  .Case({25, 300000, 10});

#endif

////////////////////////////////////////////////////////////////////////////////

int main() {
  // Use lock-free adversary
  twist::fault::SetAdversary(twist::fault::CreateLockFreeAdversary());

  RunTests(ListAllTests());
}
