#pragma once

#include "semaphore.hpp"

#include <twist/support/compiler.hpp>

#include <deque>

namespace solutions {

template <typename T>
class BufferedChannel {
 public:
  explicit BufferedChannel(size_t capacity) {
  }

  void Send(T item) {
    // Your code goes here
    items_.push_back(std::move(item));
  }

  T Recv() {
    // Your code goes here
    T item = items_.front();
    items_.pop_front();
    return item;
  }

 private:
  std::deque<T> items_;
  // Use only semaphores here
};

}  // namespace solutions

